//
//  Petition.swift
//  Project07
//
//  Created by Ruben Dias on 15/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}

