//
//  Petitions.swift
//  Project07
//
//  Created by Ruben Dias on 15/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

struct Petitions: Codable {
    var results: [Petition]
}
